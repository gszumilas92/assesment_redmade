import { defineStore } from "pinia";
import { computed, ref, watch } from "vue";
import {
  StrengthOption,
  StrengthOptionLabel,
} from "@/domain/password/strength-options";
import { RULE } from "@/domain/password/rules";

export const useStrongPasswordStore = defineStore("strong_password", () => {
  const password = ref("");
  const passwordRules = [
    [RULE.OneLetter, /[a-zA-Z]/],
    [RULE.UpperAndLower, /(?=.*[a-z])(?=.*[A-Z])/],
    [RULE.OneNumber, /\d/],
    [RULE.SpecialSymbol, /[^a-zA-Z0-9]/],
    [RULE.LongerThan4, /.{5,}/],
    [RULE.LongerThan8, /.{9,}/],
    [RULE.LongerThan12, /.{13,}/],
  ];

  const passedRules = ref([]);

  watch(password, () => {
    passedRules.value = passwordRules
      .filter(([, regex]) => regex.test(password.value))
      .map(([rule]) => rule);
  });

  const passwordStrength = computed(() =>
    passedRules.value.length >= 5 ? StrengthOption.Strong : StrengthOption.Weak
  );

  const passwordStrengthLabel = computed(
    () => StrengthOptionLabel[passwordStrength.value]
  );

  return { passwordStrength, passwordStrengthLabel, password, passedRules };
});
